# 순차적 데이터를 위한 순환 신경망 <sup>[1](#footnote_1)</sup>

> <font size="3">순환 신경망이 순차적 데이터 처리에 어떻게 사용되는지 알아본다.</font>

## 목차

1. [개요](./recurrent-neural-networks-for-sequential-data.md#intro)
1. [순환 신경망이란?](./recurrent-neural-networks-for-sequential-data.md#sec_02)
1. [순환 신경망의 작동 방식](./recurrent-neural-networks-for-sequential-data.md#sec_03)
1. [순환 신경망의 종류](./recurrent-neural-networks-for-sequential-data.md#sec_04)
1. [순환 신경망의 어플리케이션](./recurrent-neural-networks-for-sequential-data.md#sec_05)
1. [순환 신경망의 문제점과 한계](./recurrent-neural-networks-for-sequential-data.md#sec_06)
1. [요약](./recurrent-neural-networks-for-sequential-data.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 10 — Recurrent Neural Networks for Sequential Data](https://ai.gopubby.com/dl-tutorial-10-recurrent-neural-networks-for-sequential-data-9048162e3ccf?sk=6d9b4c3b0f72b5a3aff2bd9266079904)를 편역하였습니다.
